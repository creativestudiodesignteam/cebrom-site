<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Team;
use App\Models\Entities\Unit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class TeamType extends Models {
    protected $table = 'team_type';
    protected $primaryKey = 'idtype';
    public $timestamps = false;
    protected $fillable = ['title', 'status'];
    protected $guarded = [];

    public function team()
    {
        return $this->hasMany(Team::class, $this->primaryKey);
    }

    public function units()
    {
        return $this->hasMany(Unit::class, $this->primaryKey);
    }

}
