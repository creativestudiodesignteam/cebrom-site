<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\SiteContato;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Rh extends Models {
    protected $table = 'rh';
    protected $primaryKey = 'idrh';
    public $timestamps = false;
    protected $fillable = ['title', 'status'];
    protected $guarded = [];

    public function contacts()
    {
        return $this->hasMany(SiteContato::class, $this->primaryKey);
    }

}
