<?php

namespace App\Models\Entities;

use App\Core\Models;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteMidia extends Models {
    protected $table = 'site_midia';
    protected $primaryKey = 'idmedia';
    public $timestamps = false;
    protected $fillable = [ 'title', 'text', 'email', 'date_create', 'image', 'link', 'status', 'image_title', 'image_alt'];
    protected $guarded = [];

}
