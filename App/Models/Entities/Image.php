<?php

namespace App\Models\Entities;

use App\Core\Models;

class Image extends Models
{
    protected $table = 'site_images';
    protected $primaryKey = 'idimage';
    public $timestamps = false;
    protected $fillable = [ 'image', 'idpk', 'typepk', 'image_title', 'image_alt'];
    protected $guarded = [];
}
