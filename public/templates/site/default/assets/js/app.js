function hoverimg(id, style){
  $(id).attr('style', style)
}

function register(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      success: function (data) {
        var response = data['response'];
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  }

}

function alertmsg(msg, classe) {
  $('#status_response').addClass('d-none');
  $('#response').html('<div class="alert-dismissible fade show p-2" role="alert">' + msg + '</div>');
  $('#response').removeClass('alert alert-danger');
  $('#response').removeClass('alert alert-success');
  $('#response').addClass('alert ' + classe);
  $("#response").slideDown();
  setTimeout(function () {
    $("#response").slideUp();
  }, 4000);
}

$(document).ready(function () {
    $('.owl-structproject').owlCarousel({
      loop:true,
      margin:10,
      nav: true,
      dots: false,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
        }
      }
  })


    $('.owl-about').owlCarousel({
      loop:true,
      margin:10,
      nav: true,
      dots: false,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:2
          },
          1000:{
              items:3
        }
      }
  })
  
  $('.owl-unistimg').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  })

  $('.owl-units').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 2
      }
    }
  });

    $('.owl-team2').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    })

  $('.owl-team').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  })

  $(".owl-carousel").owlCarousel({
    loop: true,
    items: 1,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    autoplayTimeout: 7000,
    autoplayHoverPause: true,
    animateIn: 'fadeInUp',
    /* animateOut: 'fadeInDown', */
    transitionStyle: 'fadeIn',
    nav: true,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]

  });

  dotcount = 1;
  jQuery('.owl-dot').each(function () {
    if (dotcount < 5) {
      jQuery(this).addClass('dotnumber' + dotcount);
      jQuery(this).attr('data-info', dotcount);
      dotcount = dotcount + 1;
    }
  });

  slidecount = 1;

  jQuery('.owl-item').not('.cloned').each(function () {
    jQuery(this).addClass('slidenumber' + slidecount);
    slidecount = slidecount + 1;
  });

  jQuery('.owl-dot').each(function () {
    grab = jQuery(this).data('info');
    slidegrab = jQuery('.slidenumber' + grab + ' img').attr('alt');
    jQuery(this).css("background-image", "url(" + slidegrab + ")");
  });

  amount = $('.owl-dot').length;
  gotowidth = 100 / amount;
  //jQuery('.owl-dot').css("height", gotowidth + "%");
  jQuery('.owl-dot').css("height", "94px");
  //jQuery('.owl-dot').css("margin-bottom:", "10px");

});

$('#clients-slide').owlCarousel({
  loop: true,
  margin: 10,
  responsiveClass: true,
  autoplay: true,
  autoplayTimeout: 4000,
  autoplayHoverPause: true,
  center: true,
  dots: true,
  responsive: {
    0: {
      items: 2,
      nav: false
    },
    600: {
      items: 3,
      nav: false
    },
    1000: {
      items: 5,
      nav: true,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
    }
  }
});


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

$(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll >= 50) {
    $(".navbar").addClass("fixed-top");
    $(".navbar").addClass("bg-scroll");
  } else {
    $(".navbar").removeClass("fixed-top");
    $(".navbar").removeClass("bg-scroll");
  }
});

var $doc = $('html, body');
$('a').click(function () {
  $doc.animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
  return false;
});

particlesJS('particles-js', {
  "particles": {
    "number": {
      "value": 123,
      "density": {
        "enable": true,
        "value_area": 710.2665077774184
      }
    },
    "color": {
      "value": "#ffffff"
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#000000"
      },
      "polygon": {
        "nb_sides": 5
      },
      "image": {
        "src": "img/github.svg",
        "width": 100,
        "height": 100
      }
    },
    "opacity": {
      "value": 0.14994515164189945,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 3,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 40,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 150,
      "color": "#ffffff",
      "opacity": 0.18940440207397827,
      "width": 1
    },
    "move": {
      "enable": true,
      "speed": 1.5,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 600,
        "rotateY": 1200
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": false,
        "mode": "bubble"
      },
      "onclick": {
        "enable": false,
        "mode": "push"
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 400,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 8,
        "speed": 3
      },
      "repulse": {
        "distance": 200,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
});