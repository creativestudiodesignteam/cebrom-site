<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Core;

/**
 * Description of Action
 *
 * @author oseas
 */
class Action {

    protected $view;
    protected $action;
    protected $folder;
    protected $template = 'Default';
    protected $page;
    protected $typeFile = 'phtml';

    public function __construct() {
        $this->view = new \stdClass();
    }

    public function render($action, $folder = 'Site', $page = 'layout', $layout = true) {
        $this->action = $action;
        $this->folder = $folder;
        $this->page = $page;

        if($layout == true && file_exists('../App/Views/'.$folder.'/'.$page.'.'.$this->typeFile)){
            $this->layout();
        }else if($layout == false){
            $this->content();
        }

    }

    public function redirect($url){
       $url_ = str_replace('.', '/', $url);
       header("Location: /".$url_);
       exit();
    }

    public function layout() {
        include_once '../App/Views/'.$this->folder.'/'.$this->page.'.'.$this->typeFile;
    }

    public function content() {
        $atual = get_class($this);
        $singleClassName = str_replace("App\\Controllers\\".$this->folder."\\", "", $atual);

        include_once '../App/Views/'.$this->folder.'/'.'/'.$singleClassName.'/'.$this->action.'.'.$this->typeFile;
    }

    public function loadPage($page){
        include_once '../App/Views/'.$this->folder.'/Includes/'.$page.'.'.$this->typeFile;
    }

    public function includepage($url){
        $url_ = str_replace('.', '/', $url);
        $url_ = '/'.$url_;
        include_once '../App/Views/'.$url_.'.'.$this->typeFile;
    }

}
