<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\SiteContato;
use \App\Classes\Mail;
use App\Models\Entities\Rh;


/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Contato extends Common{

    protected $pagelink = '';
    protected $title = 'Contato';
    protected $titlesub = 'fale conosco';
    protected $pagename = '';
    protected $bread = '';

    public function index($t = '') {
        $this->start_session();
        $this->view->page = 'header-contato';
        $this->view->titlehead = 'Contato';

        if($t <> ''){
          $this->titlesub = 'Área de RH';
          $this->view->titlehead = 'Área de RH';
          $this->view->typecontact = 'rh';
          $this->view->rh = Rh::where('status', '=', 'a')->get();
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function enviar(){
      if($_POST){
        $contact = new SiteContato();
        $contact->name = $_POST['name'];
        $contact->email = $_POST['email'];
        $contact->phone = $_POST['phone'];
        $contact->fax = $_POST['fax'];
        $contact->mobilephone = $_POST['mobilephone'];
        $contact->subject = $_POST['subject'];
        $contact->idrh = isset($_POST['idrh']) ? $_POST['idrh'] : '';
        $contact->message = $_POST['message'];
        $contact->date_create = date('Y-m-d H:i:s');
        $contact->view = 'n';

        $contact->save();

        $email[] = array('name' => $this->datasite['config']->title, 'email' => $this->datasite['config']->emailsend);
        if(isset($_POST['idrh'])){
          $email[] = array('name' => $contact->rh->title, 'email' => $contact->rh->email);
        }

        $assunto = $contact->idrh <> '' ? 'Vaga para: ' . $contact->rh->title : 'Novo contato do site!';

        $mail = new Mail($this->datasite['config']->emailuser, $this->datasite['config']->emailpassword, $this->datasite['config']->smtp, $this->datasite['config']->port);
        $mail->fromname = $this->datasite['config']->title;
        $mail->recipient = $email;
        $mail->subject = addslashes(utf8_decode($this->datasite['config']->title . ' | '.$assunto));
        $mail->body = utf8_decode('
                        <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                        <b>Dados do Contato:</b><br><br>
                        <b>Data: '.$contact->date_create.'</b><br>
                        <b>Data: '.isset($contact->rh->title) ? 'Vaga para: ' . $contact->rh->title : 'Contato do Site'.'</b><br>
                        <b>Nome: '.$contact->name.'</b><br>
                        <b>E-mail: '.$contact->email.'</b><br>
                        <b>Telefone: '.$contact->phone.'</b><br>
                        <b>Celular: '.$contact->mobilephone.'</b><br>
                        <b>Fax: '.$contact->fax.'</b><br>
                        <b>Assunto: '.$contact->subject.'</b><br>
                        <b>Mensagem: '.$contact->message.'</b><br>
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse;" align="center">
                        <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center; margin-top: 10px">
                        </div>
                        </td>
                        </tr>
                ');

        $mail->altbody = utf8_decode('CEBROM | '.$assunto);

        $send = $mail->send();

        if($send){
            $return['response']['mensagem'] = 'Mensagem enviada com sucesso!';
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['redirect'] = isset($_POST['idrh']) ? '/rh' : '/contato';
        }else{
            $return['response']['mensagem'] = 'Não foi possivel enviar a mensagem!';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
        }

        print_r(json_encode($return));
        exit();
      }
    }
}
