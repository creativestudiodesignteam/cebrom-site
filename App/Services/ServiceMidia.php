<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteMidia;


class ServiceMidia{

    public function create($request) {
        if($request){
            $obj = new SiteMidia();
            $this->save($request, $obj);

            $return = [];
            if($obj->idmedia <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteMidia::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idmedia <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $departamento = SiteMidia::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->title = $request['title'];
        $obj->text = $request['text'];
        $obj->link = $request['link'];
        $obj->date_create = $request['date_create'];
        if(isset($request['image'])){
            $obj->image = $request['image'];
        }
        $obj->status = $request['status'];
        $obj->image_title = $request['image_title'];
        $obj->image_alt = $request['image_alt'];

        $obj->save();

    }

}
