<?php

namespace App\Models\Entities;

use App\Core\Models;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteFaq extends Models {
    protected $table = 'site_faq';
    protected $primaryKey = 'idfaq';
    public $timestamps = false;
    protected $fillable = [ 'title', 'text', 'porder', 'status'];
    protected $guarded = [];

}
