<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Models\Entities\AccessGroup;
use App\Models\Entities\AccessMenu;
use App\Services\ServiceGroup;

/**
 * Description of Home
 *
 * @author oseas
 */
class Groups extends Common
{

    protected $service;
    protected $pagelink = 'sistema/gruposacesso';
    protected $obj;
    protected $menus_;
    protected $id_class = 'usuarios';

    public function __construct()
    {
        $this->view = new \stdClass();
        $this->service = new ServiceGroup();
        $this->obj = new AccessGroup();
        $this->obj->status = 'a';
        $this->menus_ = AccessMenu::where('status', '=', 'a')->get();
    }

    public function index()
    {
        $this->start_session();
        $this->id_class  = 'usuarios';
        $this->titulo_pagina = 'Grupos de Acesso';

        $objs = AccessGroup::where('status', '<>', 'd')->get();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Grupos de Acessos';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/' . $this->pagelink;
        $this->view->titulo = 'Lista de Grupos';
        $this->view->objs = $objs;

        if (isset($_SESSION['message'])) {
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function create()
    {
        $this->start_session();
        $this->titulo_pagina = 'Grupos de Acesso - Cadastro';
        $this->id_class  = 'cadastrar-anuncio';


        $menus = [];
        foreach ($this->menus_ as $m) {
            $menus['menu'][] = $m;
            $menus['ckecked'][] = $this->checkmenugroup($this->obj->idgroup, $m->idmenu);
        }

        $this->view->menus = $menus;

        $breadcrumb['url'][0] = '/' . $this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Grupos de Acessos';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Cadastrar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/' . $this->pagelink;
        $this->view->action = '/' . $this->pagelink . '/create';
        $this->view->titulo = 'Lista de Grupos';
        $this->view->label = 'Cadastrar';
        $this->view->class_btn = 'btn btn-success';
        $this->view->obj = $this->obj;

        if ($_POST) {
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Grupo <strong>{$obj->description}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Grupo <strong>{$obj->description}</strong> Inserido com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }

        $this->render('form', $this->folder, $this->page);
    }

    public function update($id)
    {
        $this->start_session();
        $this->titulo_pagina = 'Grupos de Acesso - Editar';
        $this->id_class  = 'cadastrar-anuncio';
        $obj = AccessGroup::find($id);

        $menus = [];
        foreach ($this->menus_ as $m) {
            $menus['menu'][] = $m;
            $menus['ckecked'][] = $this->checkmenugroup($obj->idgroup, $m->idmenu);
        }

        $this->view->menus = $menus;

        $breadcrumb['url'][0] = '/' . $this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Grupos de Acessos';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Alterar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/' . $this->pagelink;
        $this->view->action = '/' . $this->pagelink . '/update/' . $id;
        $this->view->titulo = 'Lista de Grupos';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Grupo <strong>{$obj->description}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = "Grupo <strong>{$obj->description}</strong> alterado com sucesso!";
                $return['response']['classe'] = 'alert-success';
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function destroy($id)
    {
        $this->start_session();
        $obj = AccessGroup::find($id);
        $request = $this->service->destroy($id);

        if ($request['success']) {
            $_SESSION['message'] = "Grupo <strong>{$obj->description}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o grupo <strong>{$obj->description}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
