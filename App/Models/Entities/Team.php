<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\TeamSpecialties;
use App\Models\Entities\TeamType;
use App\Models\Entities\Unit;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Team extends Models
{
    protected $table = 'team';
    protected $primaryKey = 'idteam';
    public $timestamps = false;
    protected $fillable = [ 'image', 'title', 'status', 'text', 'certificates', 'cro', 'cv', 'idtype'];
    protected $guarded = [];


    public function type()
    {
        return $this->belongsTo(TeamType::class, 'idtype');
    }

    public function specialties()
    {
        return $this->belongsToMany(TeamSpecialties::class, 'teamxspecialties', $this->primaryKey, 'idspecialties');
    }

    public function units()
    {
        return $this->belongsToMany( Unit::class, 'teamxunit', $this->primaryKey, 'idunits');
    }
}
