<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\AccessGroup;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class AccessUser extends Models
{
    protected $table = 'access_user';
    protected $primaryKey = 'iduser';
    public $timestamps = false;
    protected $fillable = ['idgroup', 'user', 'email', 'password', 'name', 'status', 'image', 'phone', 'date_create'];
    protected $guarded = [];


    public function group()
    {
        return $this->belongsTo(AccessGroup::class, 'idgroup');
    }
}
