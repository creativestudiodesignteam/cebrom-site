<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\SiteMidia;



/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Midia extends Common{

    protected $pagelink = '';
    protected $title = 'institucional';
    protected $titlesub = 'cebrom';
    protected $pagename = 'Exames';
    protected $bread = '';

    public function index($page = 1) {
        $this->start_session();
        $this->view->page = 'header-midia';

        //paginacao
        $total = SiteMidia::where('status', '=', 'a')->count();
        //seta a quantidade de itens por página
        $registros =  8;

        //calcula o número de páginas arredondando o resultado para cima

        $numPaginas = ceil($total/$registros);

        //variavel para calcular o início da visualização com base na página atual
        $inicio = ($registros*$page) - $registros;

        $this->view->midias = SiteMidia::where('status', '=', 'a')->limit($registros)->offset($inicio)->get();
        $this->view->numPaginas = $numPaginas;
        $this->view->paginacao = $page;
        $this->view->registros = $registros;
        $this->view->max_links = 10;
        $this->view->total = $total;

        $this->view->titlehead = 'Exames';
        $this->render('index', $this->folder, $this->page);
    }
}
