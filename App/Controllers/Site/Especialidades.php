<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\TeamSpecialtieCategory;



/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Especialidades extends Common{

    protected $pagelink = '';
    protected $title = 'Especialidades';
    protected $titlesub = '';
    protected $pagename = 'Nossos Especialidades';
    protected $bread = '';

    public function index($id = '') {
        $this->start_session();
        $this->view->page = 'header-especialidades';

        if($id == ''){
            $this->view->obj = TeamSpecialtieCategory::where('status', '=', 'a')->first();
        }else{
            $this->view->obj = TeamSpecialtieCategory::find($id);
        }
        $title = explode(' ', $this->view->obj->title);
        $this->titlesub = $title[0];
        $this->pagename = $title[1];
        $this->view->titlehead = $this->view->obj->title;
        $this->view->especialities = $this->view->obj->especialities()->where('status', '=', 'a')->get();
        $this->render('index', $this->folder, $this->page);
    }

    public function more($text = '', $id = '') {
        $this->start_session();
        $this->index($id);
        exit();
    }
}
