<?php

namespace App\Models\Entities;

use App\Core\Models;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Project extends Models {
    protected $table = 'projects';
    protected $primaryKey = 'idproject';
    public $timestamps = false;
    protected $fillable = [
        'cover',
        'cover_alt',
        'cover_title',
        'logo',
        'logo_alt',
        'logo_title',
        'logo_add',
        'logo_add_alt',
        'logo_add_title',
        'whattext',
        'offeredtext',
        'image',
        'image_title',
        'image_alt',
        'participatetext',
        'goalstext',
        'contributorstext',
        'contact',
        'contact_alt',
        'contact_title', 
        'status'
    ];
    protected $guarded = [];

}
