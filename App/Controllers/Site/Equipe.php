<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\SiteTimeline;
use \App\Models\Entities\Unit;
use \App\Models\Entities\Image;
use \App\Models\Entities\TeamSpecialties;
use \App\Models\Entities\Team;
use \App\Models\Entities\TeamType;

use \App\Classes\Mail;

/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Equipe extends Common{

    protected $pagelink = '';
    protected $title = 'Equipe';
    protected $titlesub = 'conheça o';
    protected $pagename = 'Nosso Corpo Clínico';
    protected $bread = '';

    public function index() {
        $this->start_session();
        $this->view->page = 'header-equipe';
        $this->view->titlehead = 'Equipe';

        $this->view->teams = Team::where('status', '=', 'a')->get();
        $this->view->type = TeamType::first();

        $this->render('index', $this->folder, $this->page);
    }

    public function cv($text = '', $id = '') {
        $this->start_session();
        $this->view->team = Team::find($id);
        $this->view->page = 'header-equipe';
        $this->view->titlehead = $this->view->team->title;
        
        $this->pagename =  $this->view->team->title;
        $this->title = 'Médico';
        $this->titlesub = '';
        $this->bread = ' Corpo Clínico';
        
        $this->render('cv', $this->folder, $this->page);
    }

    public function area($text = '', $id = '') {
        $this->start_session();
        $this->view->page = 'header-equipe';

        if($id == ''){
            $this->index();
            exit();
        }

        $lasttype = TeamType::latest('idtype')->first();


        $this->view->teams = Team::where('status', '=', 'a')->where('idtype', '=', $id)->get();
        $this->view->type = TeamType::where('status', '=', 'a')->where('idtype', '>', $id)->first();

        if($lasttype->idtype == $id){
            $this->view->type = TeamType::where('status', '=', 'a')->first();
        }

        if(!isset($this->view->type)){
            $this->view->type = TeamType::where('status', '=', 'a')->where('idtype', '<>', $id)->first();
        }
        $this->view->titlehead = $this->view->type->title;

        $this->render('index', $this->folder, $this->page);
    }
}
