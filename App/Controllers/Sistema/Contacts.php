<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use App\Models\Entities\SiteContato;
/**
 * Description of Home
 *
 * @author oseas
 */
class Contacts extends Common{

    protected $service;
    protected $pagelink = 'sistema/contacts';
    protected $groups;
    protected $id_class = 'usuarios';
    protected $obj;


    public function index() {
        $this->start_session();

        $this->titulo_pagina = 'Contatos';
        $objs = SiteContato::all();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Contatos';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->url = '/'.$this->pagelink;
        $this->view->titulo = 'Lista de Contatos';
        $this->view->objs = $objs;

        if(isset($_SESSION['message'])){
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function view($id){

        $this->start_session();
        $this->titulo_pagina = 'Contatos - Visualizar';
        $this->id_class  = 'quem-somos';
        $obj = SiteContato::find($id);
        $obj->view = 's';
        $obj->save();

        $breadcrumb['url'][0] = '/'.$this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Contatos';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#.';
        $breadcrumb['label'][1] = 'Contato / Ler contato';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = $this->pagelink;
        $this->view->action = '/'.$this->pagelink.'/view/'.$id;
        $this->view->titulo = 'Lista de Contatos';
        $this->view->label = 'Responder';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;


        $this->render('form', $this->folder, $this->page);
    }

    public function destroy($id)
    {
        $this->start_session();
        $obj = SiteContato::find($id);
        $title = $obj->name;
        $request = $obj->delete();

        if ($request) {
            $_SESSION['message'] = "Contato <strong>{ $title }</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o Contato <strong>{ $title }</strong>";
            $_SESSION['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
        }

        $return['response']['mensagem'] = $_SESSION['message'];
        $return['response']['classe'] = $_SESSION['classe'];
        $return['response']['redirect'] = '/' . $this->pagelink;

        print_r(json_encode($request));
        exit();
    }
}
