<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\Treatment;
use \App\Models\Entities\Image;



/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Tratamentos extends Common{

    protected $pagelink = '';
    protected $title = 'Tratamentos';
    protected $titlesub = '';
    protected $pagename = 'Nossos Tratamentos';
    protected $bread = '';

    public function index($id = '') {
        $this->start_session();
        $this->view->page = 'header-tratamento';

        if($id == ''){
            $this->view->treatment = Treatment::where('status', '=', 'a')->first();
        }else{
            $this->view->treatment = Treatment::find($id);
        }

        $this->bg = 'style="background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(\''.$this->view->treatment->cover.'\') no-repeat cover;"';
        $this->pagename = $this->view->treatment->title;
        $this->view->structs = Image::where( 'idpk', '=', $this->view->treatment->idtreatments)->where( 'typepk', '=', 't')->get();
        $this->view->faq = $this->view->treatment->faqs()->orderBy('porder')->get();
        $this->view->titlehead = $this->view->treatment->title;
        $this->render('index', $this->folder, $this->page);
    }

    public function more($text = '', $id = '') {
        $this->start_session();
        $this->index($id);
        exit();
    }
}
