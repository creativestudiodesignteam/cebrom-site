<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use App\Models\Entities\Project;
use App\Models\Entities\SiteMidia;
use App\Models\Entities\Team;

/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Search extends Common{

    protected $pagelink = '';
    protected $title = 'Busca';
    protected $titlesub = 'resultados';
    protected $pagename = '';
    protected $bread = '';

    public function index($t = '') {
        $this->start_session();
        $this->view->page = 'header-contato';
        $this->view->titlehead = 'Busca';

        if(isset($_GET['s'])){
          $this->titlesub = 'Resultados para sua busca "'.$_GET['s'].'"';
          $this->view->teams = Team::where('status', '=', 'a')->where('title', 'like', '%' . $_GET['s'] . '%')->orWhere('title', 'like', '%' . strtoupper($_GET['s']) . '%')->get();
          $this->view->projects = Project::where('status', '=', 'a')->where('title', 'like', '%' . $_GET['s'] . '%')->orWhere('title', 'like', '%' . strtoupper($_GET['s']) . '%')->get();
          $this->view->exames = SiteMidia::where('status', '=', 'a')->where('title', 'like', '%' . $_GET['s'] . '%')->orWhere('title', 'like', '%' . strtoupper($_GET['s']) . '%')->get();
          $this->view->search = $_GET['s'];
        }

        $this->render('index', $this->folder, $this->page);
    }

    
}
