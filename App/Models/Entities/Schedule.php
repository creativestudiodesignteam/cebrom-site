<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\ScheduleCategories;

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Schedule extends Models
{
    protected $table = 'schedule';
    protected $primaryKey = 'idschedule';
    public $timestamps = false;
    protected $fillable = [ 'image', 'image_title', 'image_alt', 'title', 'status', 'text', 'date', 'idcategories'];
    protected $guarded = [];


    public function category()
    {
        return $this->belongsTo(ScheduleCategories::class, 'idcategories');
    }
}
