<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\SiteTimeline;
use \App\Models\Entities\Unit;
use \App\Models\Entities\Image;
use \App\Models\Entities\TeamSpecialties;
use \App\Models\Entities\Team;


use \App\Classes\Mail;

/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Empresa extends Common{

    protected $pagelink = '';
    protected $title = 'Empresa';
    protected $titlesub = 'conheça';
    protected $pagename = 'Nossa História';
    protected $bread = '';

    public function index() {
        $this->start_session();
        $this->view->page = 'header-empresa';
        $this->view->titlehead = 'Empresa';
        $this->view->timeline = SiteTimeline::where('status', '=', 'a')->get();

        $this->render('index', $this->folder, $this->page);
    }

    public function unidades($text = '', $id = '') {
        $this->start_session();
        $this->view->page = 'header-unidade';
        $this->view->titlehead = 'Unidades';

        if($id <> ''){
          $this->view->unit = Unit::find($id);
        }else{
          $this->view->unit = Unit::first();
        }

        $this->view->structs = Image::where( 'idpk', '=', $this->view->unit->idunits)->where( 'typepk', '=', 'u')->get();
        $this->view->specialities = TeamSpecialties::where('idtype', '=', $this->view->unit->idtype)->get();
        $this->view->teams = Team::where('idtype', '=', $this->view->unit->idtype)->get();
        
        $this->pagename =  $this->view->unit->title;
        $this->titlesub = 'unidade I';
        $this->bread = ' Unidade';

        $this->render('unidade', $this->folder, $this->page);
    }
}
