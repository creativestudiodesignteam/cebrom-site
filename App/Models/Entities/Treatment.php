<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\TreatmentFaq;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class Treatment extends Models {
    protected $table = 'treatments';
    protected $primaryKey = 'idtreatments';
    public $timestamps = false;
    protected $fillable = [ 'cover', 'text', 'image', 'image_title', 'image_alt', 'doctor', 'cover_title', 'cover_alt', 'doctor_alt', 'doctor_title', 'status'];
    protected $guarded = [];

    public function faqs()
    {
        return $this->hasMany(TreatmentFaq::class, $this->primaryKey);
    }

   
}
