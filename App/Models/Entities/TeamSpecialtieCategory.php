<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\TeamSpecialties;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class TeamSpecialtieCategory extends Models {
    protected $table = 'team_specialties_categories';
    protected $primaryKey = 'idcategory';
    public $timestamps = false;
    protected $fillable = ['title', 'status', 'color', 'text', 'icon'];
    protected $guarded = [];

    public function especialities()
    {
        return $this->hasMany(TeamSpecialties::class, $this->primaryKey);
    }

}
