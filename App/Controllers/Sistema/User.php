<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Models\Entities\AccessUser;
use \App\Models\Entities\AccessGroup;
use \App\Services\ServiceAccessUser;

/**
 * Description of Home
 *
 * @author oseas
 */
class User extends Common
{

    protected $service;
    protected $cservice;
    protected $pagelink = 'sistema/users';
    protected $groups;
    protected $clients;
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct()
    {
        $this->start_session();
        $this->view = new \stdClass();
        $this->service = new ServiceAccessUser();
        $this->obj = new AccessUser();
        $this->obj->status = 'a';

        $this->groups = AccessGroup::where('status', '=', 'a')->get();
        $this->view->groups = $this->groups;
    }

    public function index()
    {
        $this->start_session();

        $this->titulo_pagina = 'Usuários';
        $objs = AccessUser::where('status', '<>', 'd')->get();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Usuários';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->url = '/' . $this->pagelink;
        $this->view->titulo = 'Lista de Usuários';
        $this->view->objs = $objs;
        $this->view->colors = array('-purple', '-green');

        if (isset($_SESSION['message'])) {
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function create()
    {
        $this->start_session();
        $this->titulo_pagina = 'Usuário - Cadastro';
        $this->id_class = 'cadastrar-anuncio';

        $breadcrumb['url'][0] = '/' . $this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Usuários';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#.';
        $breadcrumb['label'][1] = 'Cadastrar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/' . $this->pagelink;
        $this->view->action = '/' . $this->pagelink . '/create';
        $this->view->titulo = 'Lista de Usuários';
        $this->view->label = 'Cadastrar';
        $this->view->class_btn = 'btn btn-success';
        $this->view->obj = $this->obj;


        if ($_POST) {
            $_POST['date_create'] = date('Y-m-d H:i:s');


            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function update($id)
    {
        $this->start_session();
        $this->titulo_pagina = 'Usuário - Editar';
        $this->id_class = 'cadastrar-anuncio';
        $obj = AccessUser::find($id);

        $breadcrumb['url'][0] = '/' . $this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Usuários';
        $breadcrumb['active'][0] = 'active';
        $breadcrumb['url'][1] = '#.';
        $breadcrumb['label'][1] = 'Alterar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = $this->pagelink;
        $this->view->action = '/' . $this->pagelink . '/update/' . $id;
        $this->view->titulo = 'Lista de Usuários';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;

        if ($_POST) {


            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;

                print_r(json_encode($return));
                exit();
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';

                print_r(json_encode($return));
                exit();
            }
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function destroy($id)
    {
        $this->start_session();
        $obj = AccessUser::find($id);
        $request = $this->service->destroy($id);

        $crequest = $this->cservice->destroy($obj->idclient);

        if ($request['success']) {
            $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> excluido com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel excluir o usuário <strong>{$obj->name}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }

    public function block($id, $s)
    {
        $this->start_session();
        $obj = AccessUser::find($id);
        $obj->status = $s;
        $request = $obj->save();

        if ($request) {
            $_SESSION['message'] = "Usuário <strong>{$obj->name}</strong> bloqueado com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        } else {
            $_SESSION['message'] = "Não foi possivel bloqueado o usuário <strong>{$obj->name}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();
    }
}
