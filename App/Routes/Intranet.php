<?php

namespace App\Routes;

use \App\Core\Routes;

\Slim\Slim::registerAutoloader();

class Intranet extends Routes
{

    protected $routes = [];
    protected $routescrud = [];

    public function __construct()
    {
        $app = new \Slim\Slim(array(
            'templates.path' => 'templates'
        ));

        // rotas index
        $this->routes = [
            '/',
            '/home',
            '/index',
            '/dashboard'
        ];

        //rotas sistema
        $app->group('/sistema', function () use ($app) {
            foreach ($this->routes as $route) {
                $app->get($route, function () use ($app) {
                    $this->initroute('Sistema', 'Home', 'index');
                });
            }

            $app->map('/perfil/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Sistema', 'Home', 'perfil', $data);
            })->via('GET', 'POST');

            $app->map('/uploads', function () use ($app) {
                $this->initroute('Sistema', 'Home', 'uploads');
            })->via('GET', 'POST');

            //rotas login
            $app->group('/login', function () use ($app) {

                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Login', 'index');
                });

                $app->post('/logar', function () use ($app) {
                    $request = $app->request->getBody();
                    parse_str($request, $get_array);

                    $data[] = $get_array;

                    $this->initroute('Sistema', 'Login', 'logar', $data);
                });

                $app->post('/retrievepassword', function () use ($app) {
                    $request = $app->request->getBody();
                    parse_str($request, $get_array);

                    $data[] = $get_array;

                    $this->initroute('Sistema', 'Login', 'retrievepassword', $data);
                });

                $app->get('/logout', function () use ($app) {
                    $this->initroute('Sistema', 'Login', 'logout');
                });
            });

            //rotas settings
            $app->group('/settings', function () use ($app) {
                $app->map('/', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'index');
                })->via('GET', 'POST');

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Settings', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Settings', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Settings', 'destroy', $data);
                });
            });

            //rotas grupos
            $app->group('/gruposacesso', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'index');
                });


                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Groups', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Groups', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Groups', 'destroy', $data);
                });
            });

            //rotas menus
            $app->group('/menus', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'index');
                });


                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Menus', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Menus', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Menus', 'destroy', $data);
                });
            });

            //rotas users
            $app->group('/users', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'index');
                });


                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'User', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'User', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'User', 'destroy', $data);
                });

                $app->get('/block/:id/:s', function ($id, $s) use ($app) {
                    $data[] = $id;
                    $data[] = $s;
                    $this->initroute('Sistema', 'User', 'block', $data);
                });
            });

            //rotas slides
            $app->group('/slides', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Slide', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Slide', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Slide', 'destroy', $data);
                });
            });

            //rotas timeline
            $app->group('/timeline', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Timeline', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Timeline', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Timeline', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Timeline', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Timeline', 'destroy', $data);
                });
            });

            //rotas units
            $app->group('/units', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Units', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Units', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Units', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Units', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Units', 'uploads');
                })->via('GET', 'POST');

                $app->map('/uploadsimages', function () use ($app) {
                    $this->initroute('Sistema', 'Units', 'uploadsimages');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Units', 'destroy', $data);
                });

                $app->map( '/deleteimage', function () use ($app) {
                    $this->initroute('Sistema', 'Units', 'deleteimage');
                })->via('GET', 'POST');
            });

            //rotas faq
            $app->group( '/faq', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Faq', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Faq', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Faq', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Faq', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Faq', 'destroy', $data);
                });
            });

            //rotas seo
            $app->group('/seo', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Seo', 'index');
                })->via('GET', 'POST');

                $app->map('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Seo', 'index');
                })->via('GET', 'POST');
            });

            //rotas contacts
            $app->group('/contacts', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Contacts', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Contacts', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Contacts', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Contacts', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/view/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Contacts', 'view', $data);
                })->via('GET', 'POST');


                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Contacts', 'destroy', $data);
                });
            });

            //rotas midia
            $app->group('/midia', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Midia', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Midia', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Midia', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Midia', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Midia', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Midia', 'destroy', $data);
                });
            });

            //rotas treatments
            $app->group('/treatments', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Treatments', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Treatments', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Treatments', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Treatments', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Treatments', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Treatments', 'destroy', $data);
                });

                $app->map('/deleteimage', function () use ($app) {
                    $this->initroute('Sistema', 'Treatments', 'deleteimage');
                })->via('GET', 'POST');

                $app->map('/uploadsimages', function () use ($app) {
                    $this->initroute('Sistema', 'Treatments', 'uploadsimages');
                })->via('GET', 'POST');

                //rotas faq
                $app->group('/faq', function () use ($app) {
                    $app->get('/', function () use ($app) {
                        $this->initroute('Sistema', 'TreatmentsFaq', 'index');
                    });

                    $app->get('/index', function () use ($app) {
                        $this->initroute('Sistema', 'TreatmentsFaq', 'index');
                    });

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'TreatmentsFaq', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'TreatmentsFaq', 'update', $data);
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'TreatmentsFaq', 'destroy', $data);
                    });
                });
            });

            //rotas newsletter
            $app->group('/newsletter', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'News', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'News', 'index');
                });

                $app->get('/export', function () use ($app) {
                    $this->initroute('Sistema', 'News', 'export');
                });

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'News', 'destroy', $data);
                });
            });

            //rotas team
            $app->group( '/team', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Teams', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'uploads');
                })->via('GET', 'POST');

                $app->map('/uploadfile', function () use ($app) {
                    $this->initroute('Sistema', 'Teams', 'uploadfile');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Teams', 'destroy', $data);
                });


                //rotas specialties
                $app->group('/specialties', function () use ($app) {
                    $app->get('/', function () use ($app) {
                        $this->initroute('Sistema', 'TeamSpecialtie', 'index');
                    });

                    $app->get('/index', function () use ($app) {
                        $this->initroute('Sistema', 'TeamSpecialtie', 'index');
                    });

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'TeamSpecialtie', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'TeamSpecialtie', 'update', $data);
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'TeamSpecialtie', 'destroy', $data);
                    });

                    //rotas categories
                    $app->group('/categories', function () use ($app) {
                        $app->get('/', function () use ($app) {
                            $this->initroute('Sistema', 'TeamSpecialtieCategories', 'index');
                        });

                        $app->get('/index', function () use ($app) {
                            $this->initroute('Sistema', 'TeamSpecialtieCategories', 'index');
                        });

                        $app->map('/create', function () use ($app) {
                            $this->initroute('Sistema', 'TeamSpecialtieCategories', 'create');
                        })->via('GET', 'POST');

                        $app->map('/update/:id', function ($id) use ($app) {
                            $data[] = $id;
                            $this->initroute('Sistema', 'TeamSpecialtieCategories', 'update', $data);
                        })->via('GET', 'POST');

                        $app->map('/uploads', function () use ($app) {
                            $this->initroute('Sistema', 'TeamSpecialtieCategories', 'uploads');
                        })->via('GET', 'POST');


                        $app->get('/destroy/:id', function ($id) use ($app) {
                            $data[] = $id;
                            $this->initroute('Sistema', 'TeamSpecialtieCategories', 'destroy', $data);
                        });
                    });

                });

                //rotas types
                $app->group( '/types', function () use ($app) {
                    $app->get('/', function () use ($app) {
                        $this->initroute('Sistema', 'TeamTypes', 'index');
                    });

                    $app->get('/index', function () use ($app) {
                        $this->initroute('Sistema', 'TeamTypes', 'index');
                    });

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'TeamTypes', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'TeamTypes', 'update', $data);
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'TeamTypes', 'destroy', $data);
                    });
                });
            });

            //rotas schedules
            $app->group('/schedules', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Schedules', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Schedules', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Schedules', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Schedules', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Schedules', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Schedules', 'destroy', $data);
                });

                //rotas categories
                $app->group( '/categories', function () use ($app) {
                    $app->get('/', function () use ($app) {
                        $this->initroute('Sistema', 'ScheduleCategories', 'index');
                    });

                    $app->get('/index', function () use ($app) {
                        $this->initroute('Sistema', 'ScheduleCategories', 'index');
                    });

                    $app->map('/create', function () use ($app) {
                        $this->initroute('Sistema', 'ScheduleCategories', 'create');
                    })->via('GET', 'POST');

                    $app->map('/update/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ScheduleCategories', 'update', $data);
                    })->via('GET', 'POST');

                    $app->get('/destroy/:id', function ($id) use ($app) {
                        $data[] = $id;
                        $this->initroute('Sistema', 'ScheduleCategories', 'destroy', $data);
                    });
                });
            });

            //rotas projects
            $app->group('/projects', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Projects', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Projects', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Projects', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Projects', 'update', $data);
                })->via('GET', 'POST');

                $app->map('/uploads', function () use ($app) {
                    $this->initroute('Sistema', 'Projects', 'uploads');
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Projects', 'destroy', $data);
                });

                $app->map('/deleteimage', function () use ($app) {
                    $this->initroute('Sistema', 'Projects', 'deleteimage');
                })->via('GET', 'POST');

                $app->map('/uploadsimages', function () use ($app) {
                    $this->initroute('Sistema', 'Projects', 'uploadsimages');
                })->via('GET', 'POST');
            });

            //rotas rh
            $app->group('/rh', function () use ($app) {
                $app->get('/', function () use ($app) {
                    $this->initroute('Sistema', 'Rhs', 'index');
                });

                $app->get('/index', function () use ($app) {
                    $this->initroute('Sistema', 'Rhs', 'index');
                });

                $app->map('/create', function () use ($app) {
                    $this->initroute('Sistema', 'Rhs', 'create');
                })->via('GET', 'POST');

                $app->map('/update/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Rhs', 'update', $data);
                })->via('GET', 'POST');

                $app->get('/destroy/:id', function ($id) use ($app) {
                    $data[] = $id;
                    $this->initroute('Sistema', 'Rhs', 'destroy', $data);
                });
            });


        });

        $app->run();
    }
}
