<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteParceiro;


class ServiceParceiro{

    public function create($request) {
        if($request){
            $obj = new SiteParceiro();
            $this->save($request, $obj);

            $return = [];
            if($obj->idpartners <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o parceiro <strong>{$obj->name}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteParceiro::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idpartners <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o parceiro <strong>{$obj->name}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $departamento = SiteParceiro::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->name = $request['name'];
        $obj->url = $request['url'];
        if(isset($request['image'])){
            $obj->image = $request['image'];
        }
        $obj->status = $request['status'];

        $obj->save();

    }

}
