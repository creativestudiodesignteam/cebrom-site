<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\Project;
use \App\Models\Entities\Image;



/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Projetos extends Common{

    protected $pagelink = '';
    protected $title = 'Projetos';
    protected $titlesub = '';
    protected $pagename = 'Nossos Projetos';
    protected $bread = '';

    public function index($id = '') {
        $this->start_session();
        $this->view->page = 'header-projeto';

        if($id == ''){
            $this->view->obj = Project::where('status', '=', 'a')->first();
        }else{
            $this->view->obj = Project::find($id);
        }

        $this->bg = 'style="background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(\''.$this->view->obj->cover.'\') no-repeat cover;"';
        $this->pagename = $this->view->obj->title;
        $this->view->structs = Image::where( 'idpk', '=', $this->view->obj->idproject)->where( 'typepk', '=', 'p')->get();
        $this->view->titlehead = $this->view->obj->title;
        $this->render('index', $this->folder, $this->page);
    }

    public function more($text = '', $id = '') {
        $this->start_session();
        $this->index($id);
        exit();
    }
}
