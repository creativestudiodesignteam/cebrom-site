<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteFaq;


class ServiceFaq{

    public function create($request) {
        if($request){
            $obj = new SiteFaq();
            $this->save($request, $obj);

            $return = [];
            if($obj->idfaq <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Faq <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteFaq::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idfaq <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Faq <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $departamento = SiteFaq::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->text = $request['text'];
        $obj->title = $request['title'];
        $obj->porder = $request['porder'];
        $obj->status = $request['status'];

        $obj->save();

    }

}
