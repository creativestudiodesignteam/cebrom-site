<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use App\Models\Entities\SiteNews;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
/**
 * Description of Home
 *
 * @author oseas
 */
class News extends Common{

    protected $service;
    protected $pagelink = 'sistema/newsletter';
    protected $obj;


    public function index() {
        $this->start_session();

        $this->titulo_pagina = 'News';
        $objs = SiteNews::all();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de News';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->url = '/'.$this->pagelink;
        $this->view->titulo = 'Lista de News';
        $this->view->objs = $objs;

        if(isset($_SESSION['message'])){
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function destroy($id){
        $this->start_session();
        $email = SiteNews::find($id);
        $obj = SiteNews::where('idnewsletter', '=', $id)->delete();

        if($obj){
            $_SESSION['message'] = "E-mail <strong>{$email->email}</strong> excluida com sucesso!";
            $_SESSION['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $_SESSION['message'] = "Não foi possivel excluir o E-mail <strong>{$email->email}</strong>";
            $_SESSION['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
        }

        $return['response']['mensagem'] =$_SESSION['message'];
        $return['response']['classe'] = $_SESSION['classe'];

        print_r(json_encode($return));
        exit();

    }

    public function export(){

        ini_set('memory_limit', '-1');
        set_time_limit(600);

        $this->view->objs = SiteNews::all();

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('Export newsletter')
            ->setLastModifiedBy( 'Export newsletter')
            ->setTitle('Export newsletter')
            ->setSubject('Export newsletter')
            ->setDescription('Export newsletter')
            ->setKeywords('Export newsletter')
            ->setCategory('Export newsletter');

        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'id')
            ->setCellValue('B1', 'name')
            ->setCellValue('C1', 'email')
            ->setCellValue('D1', 'date create')
            ->setCellValue('K1', 'Data');

        // Miscellaneous glyphs, UTF-8
        $row = 2;
        $col = 1;
        foreach ($this->view->objs as $a) {
            $spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, $row, $a->idnewsletter);
            $col++;
            $spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, $row, $a->name);
            $col++;
            $spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, $row, $a->email);
            $col++;
            $spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, $row, date("d/m/Y H:i:s", strtotime($a->data_create)));
            $row++;

            $col = 1;
        }


        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Export newsletter');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="newsletters' . date('Y-m-d-H-i-s') . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
