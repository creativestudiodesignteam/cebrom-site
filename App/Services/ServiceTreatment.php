<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Treatment;


class ServiceTreatment
{

    public function create($request)
    {
        if ($request) {
            $obj = new Treatment();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idtreatments <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o tratamento <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = Treatment::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idtreatments <> '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o tratamento <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $departamento = Treatment::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $obj->title = $request['title'];
        $obj->text = $request['text'];
        $obj->status = $request['status'];
        $obj->image = $request['image'];
        $obj->image_title = $request['image_title'];
        $obj->image_alt = $request['image_alt'];
        $obj->cover = $request['cover'];
        $obj->cover_title = $request['cover_title'];
        $obj->cover_alt = $request['cover_alt'];
        $obj->doctor = $request['doctor'];
        $obj->doctor_alt = $request['doctor_alt'];
        $obj->doctor_title = $request['doctor_title'];

        $obj->save();
    }
}
