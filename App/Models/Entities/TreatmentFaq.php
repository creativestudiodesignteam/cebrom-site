<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Treatment;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class TreatmentFaq extends Models {
    protected $table = 'treatments_faq';
    protected $primaryKey = 'idfaq';
    public $timestamps = false;
    protected $fillable = [ 'title', 'text', 'porder', 'status', 'idtreatments'];
    protected $guarded = [];

    public function treatment()
    {
        return $this->belongsTo(Treatment::class, 'idtreatments');
    }

}
