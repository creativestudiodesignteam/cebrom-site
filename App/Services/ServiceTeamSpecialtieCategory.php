<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\TeamSpecialtieCategory;


class ServiceTeamSpecialtieCategory{

    public function create($request) {
        if($request){
            $obj = new TeamSpecialtieCategory();
            $this->save($request, $obj);

            $return = [];
            if($obj->idcategory <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = TeamSpecialtieCategory::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idcategory <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o registro <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $registro = TeamSpecialtieCategory::find($id);
        $registro->status = 'd';
        $resp = $registro->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->title = $request['title'];
        $obj->status = $request['status'];
        $obj->text = $request['text'];
        $obj->link = $request['link'];
        $obj->icon = $request['icon'];
        $obj->color = $request['color'];
        $obj->image = $request['image'];
        $obj->image_title = $request['image_title'];
        $obj->image_alt = $request['image_alt'];


        $obj->save();

    }

}
