<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Project;


class ServiceProject
{

    public function create($request)
    {
        if ($request) {
            $obj = new Project();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idproject <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o tratamento <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = Project::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idproject <> '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o tratamento <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $departamento = Project::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $obj->title = $request['title'];
        $obj->whattext = $request['whattext'];
        $obj->offeredtext = $request['offeredtext'];
        $obj->participatetext = $request['participatetext'];
        $obj->goalstext = $request['goalstext'];
        $obj->contributorstext = $request['contributorstext'];
        $obj->contacttext = $request['contacttext'];
        $obj->status = $request['status'];
        $obj->image = $request['image'];
        $obj->image_title = $request['image_title'];
        $obj->image_alt = $request['image_alt'];
        $obj->cover = $request['cover'];
        $obj->cover_title = $request['cover_title'];
        $obj->cover_alt = $request['cover_alt'];
        $obj->logo = $request['logo'];
        $obj->logo_alt = $request['logo_alt'];
        $obj->logo_title = $request['logo_title'];
        $obj->logo_add = $request['logo_add'];
        $obj->logo_add_alt = $request['logo_add_alt'];
        $obj->logo_add_title = $request['logo_add_title'];
        $obj->contact = $request['contact'];
        $obj->contact_alt = $request['contact_alt'];
        $obj->contact_title = $request['contact_title'];

        $obj->save();
    }
}
