<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Schedule;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class ScheduleCategory extends Models {
    protected $table = 'schedule_categories';
    protected $primaryKey = 'idcategory';
    public $timestamps = false;
    protected $fillable = ['title', 'status'];
    protected $guarded = [];

    public function team()
    {
        return $this->hasMany(Schedule::class, $this->primaryKey);
    }

}
