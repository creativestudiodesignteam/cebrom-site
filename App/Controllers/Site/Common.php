<?php

namespace App\Controllers\Site;


use \App\Controllers\Site\Config;
use \App\Models\Entities\SiteRedesSociaisMotel;
use \App\Models\Entities\TeamType;
use \App\Models\Entities\TeamSpecialtieCategory;
use \App\Models\Entities\Project;
use \App\Models\Entities\Treatment;
use \App\Models\Entities\Unit;
use \App\Models\Entities\Configuration;
use \App\Models\Entities\SiteSeo;


/**
 * Description of Common
 *
 * @author oseas
 */
class Common extends Config{

    public function __construct(){
        parent::__construct();
        $this->datasite = $this->info();

    }

    public function info(){
        $info = [];
        $info['teams'] = TeamType::where('status', '=', 'a')->get();
        $info['specialtieCategory'] = TeamSpecialtieCategory::where('status', '=', 'a')->get();
        $info['treatments'] = Treatment::where('status', '=', 'a')->get();
        $info['project'] = Project::where('status', '=', 'a')->get();
        $info['units'] = Unit::where('status', '=', 'a')->get();
        $info['config'] = Configuration::find(1);
        $info['seo'] = SiteSeo::find(1);

        return $info;
    }

}
