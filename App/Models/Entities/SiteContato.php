<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Rh;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteContato extends Models {
    protected $table = 'site_contacts';
    protected $primaryKey = 'idcontact';
    public $timestamps = false;
    protected $fillable = ['name', 'phone', 'email', 'message', 'view', 'idrh'];
    protected $guarded = [];


    public function rh()
    {
        return $this->belongsTo(Rh::class, 'idrh');
    }


}
