$('form .form-control[required]').change(function () {
  if ($(this).val()) {
    $(this).removeClass('is-invalid');
  }
});

function valida(frm) {
  var valor = $(frm).val().replace(/[^a-zA-Z-0-9]+/g, '');
  $(frm).val(valor);
}

function trocaicone(icone) {
  $('#mostraicone').removeClass();
  $('#mostraicone').addClass(icone);
}


function loadregistercombobox(t, id, url) {
  let dropdown = $(id);

  dropdown.empty();
  dropdown.removeAttr('disabled');

  dropdown.append('<option value="" selected="true" disabled>Selecione</option>');
  dropdown.prop('selectedIndex', 0);

  $.getJSON(url + '/' + t.value, function (data) {
    $.each(data, function (key, entry) {
      dropdown.append($('<option></option>').attr('value', entry.id).text(entry.title));
    })
  });
}

// Evento Submit do formulário
function upload(form) {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);

  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: $('#urlpage').val() + "/uploads",
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      if (r.status == '1') {
        $('#imgload').val(r.image);
        $('#previewimg').attr('src', r.image);
      }
      //alertmsg(retorno.mensagem, retorno.class);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

// Evento Submit do formulário
function uploadreturn(form) {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);

  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: $('#urlpage').val() + "/uploads",
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      var d = '';
      if (r.status == '1') {
        //alert(r.data);
        d = 'Total de clientes importados: ' + r.data.client + '<br>';
        d += 'Total de coleções: ' + r.data.season + '<br>';
        d += 'Total de tipos: ' + r.data.type + '<br>';
        d += 'Total de categorias: ' + r.data.category + '<br>';
        d += 'Total de produtos: ' + r.data.product + '<br>';


        $('#response').html(d);
      }
      //alertmsg(retorno.mensagem, retorno.class);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function uploadmulti(form, parameters, idreturn) {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);

  var formURL = $('#'+form).attr("action");

  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: formURL + "" + parameters,
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      /* if (r.status == '1') {
        $(idreturn).append('<span><img src="' + r.image + '" style="width: 100px"></span>');
      } else {
        alertmsg(r.mensagem, r.classe);
      } */
      //alertmsg(retorno.mensagem, retorno.class);
      window.location.replace(r.redirect);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function uploadall(form, formURL) {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);
  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: formURL,
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      alert(retorno.mensagem);
      location.reload();
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function uploadgeneric(form, type_, formURL) {
  // Captura os dados do formulário
  var formulario = document.getElementById(form);
  var type_file = $(type_).val();
  // Instância o FormData passando como parâmetro o formulário
  var formData = new FormData(formulario);
  $('#status_response').removeClass('d-none');
  // Envia O FormData através da requisição AJAX
  $.ajax({
    url: formURL,
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (retorno) {
      $('#status_response').addClass('d-none');
      var r = retorno.response;
      if (r.status == '1') {
        $('#' + type_file).val(r.image);
        $('#preview' + type_file).attr('src', r.image);
      }
      //alertmsg(retorno.mensagem, retorno.class);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      $('#status_response').addClass('d-none');
      alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
      return false;
    }
  });

  return false;
}

function deleteimage(image, t) {
  var r = confirm("Tem certeza que deseja deletar está imagem?");
  if (r == true) {
    $('#status_response').removeClass('d-none');
    $.ajax({
      type: "POST",
      url: $('#urlpage').val() + "/deleteimage",
      data: 'image=' + image,
      dataType: 'json',
      success: function (data) {
        var r = data.response;
        alertmsg(r.mensagem, r.classe);
        $(t).remove();
      }
    });
  } else {
    return false;
  }
}

function deletar(url, nome, redirect = '') {
  var r = confirm("Tem certeza que deseja deletar " + nome + "?");
  if (r == true) {
    var formURL = url;
    $('#status_response').removeClass('d-none');
    $.ajax({
      type: "GET",
      url: formURL,
      dataType: 'json',
      success: function (data) {
        if (redirect == '') {
          location.reload();
        } else {
          window.location.href = redirect;
        }
      }
    });
  } else {
    return false;
  }
}

function block(url, nome) {
  var r = confirm("Tem certeza que deseja bloquear " + nome + "?");
  if (r == true) {
    var formURL = url;
    $('#status_response').removeClass('d-none');
    $.ajax({
      type: "GET",
      url: formURL,
      dataType: 'json',
      success: function (data) {
        if (data == true) {
          location.reload();
        }

      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        $('#status_response').addClass('d-none');
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  } else {
    return false;
  }
}

function register(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      success: function (data) {
        var response = data['response'];
        $('#status_response').addClass('d-none');
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        $('#status_response').addClass('d-none');
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  }

}

function alertmsg(msg, classe) {
  $('#status_response').addClass('d-none');
  $('#response').html('<div class="alert-dismissible fade show p-2" role="alert">' + msg + '</div>');
  $('#response').removeClass('alert alert-danger');
  $('#response').removeClass('alert alert-success');
  $('#response').addClass('alert ' + classe);
  $("#response").slideDown();
  setTimeout(function () {
    $("#response").slideUp();
  }, 4000);
}

$(document).keypress(function (e) {
  if (e.which == 13) {
    return false
  }
});

function login(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      success: function (data) {
        var response = data['response'];
        $('#status_response').addClass('d-none');
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      }
    });
  }
}

function enviarsenha(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  $.ajax({
    type: "POST",
    url: formURL,
    data: postData,
    dataType: 'json',
    success: function (data) {
      var response = data['response'];
      $('#responsepass').html('<div class="alert-dismissible fade show p-2" role="alert">' + response['mensagem'] + '</div>');
      $('#responsepass').removeClass('alert alert-danger');
      $('#responsepass').removeClass('alert alert-success');
      $('#responsepass').addClass('alert ' + response['classe']);


      if (response['result'] == 'error') {
        $('.validate-input').each(function () {
          if ($(this).val() == '') {

            $(this).addClass('alert-validate');
          }
        });
        return false;
      } else {
        window.location.replace(response['redirect']);
      }
    }
  });
}

function somenteNumeros(num) {
  var er = /[^0-9.]/;
  er.lastIndex = 0;
  var campo = num;
  if (er.test(campo.value)) {
    campo.value = "";
  }
}

function readURL(input, preview) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $(preview).attr('src', e.target.result)
    };
    reader.readAsDataURL(input.files[0]);
  }
  else {
    var img = input.value;
    $(input).next().attr('src', img);
  }
}

function verificaMostraBotao(input, preview) {
  $(input).each(function (index) {
    if ($(input).eq(index).val() != "") {
      readURL(this, preview);
      $('.hide').show();
    }
  });
}
