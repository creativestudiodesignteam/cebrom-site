<?php

namespace App\Routes;

use \App\Core\Routes;

\Slim\Slim::registerAutoloader();

class Web extends Routes
{

    protected $routes = [];
    protected $routescrud = [];

    public function __construct()
    {
        $app = new \Slim\Slim(array(
            'templates.path' => 'templates'
        ));

        // rotas index
        $this->routes = [
            '/',
            '/home',
            '/index'
        ];

        foreach ($this->routes as $route) {
            $app->get($route, function () use ($app) {
                $this->initroute('Site', 'Home', 'index');
            });
        }

        $app->get('/newsletter', function () use ($app) {
            $this->initroute('Site', 'Home', 'newsletter');
        })->via('GET', 'POST');

        $app->get('/empresa', function () use ($app) {
            $this->initroute('Site', 'Empresa', 'index');
        })->via('GET', 'POST');

        $app->group('/empresa', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Site', 'Empresa', 'index');
            })->via('GET', 'POST');

            $app->get('/index', function () use ($app) {
                $this->initroute('Site', 'Empresa', 'index');
            })->via('GET', 'POST');

            $app->map('/unidades', function () use ($app) {
                $this->initroute('Site', 'Empresa', 'unidades');
            })->via('GET', 'POST');

            $app->map('/unidades/:text/:id', function ($text, $id) use ($app) {
                $data[] = $text;
                $data[] = $id;
                $this->initroute('Site', 'Empresa', 'unidades', $data);
            })->via('GET', 'POST');
        });

        $app->group('/equipe', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Site', 'Equipe', 'index');
            })->via('GET', 'POST');

            $app->get('/index', function () use ($app) {
                $this->initroute('Site', 'Equipe', 'index');
            })->via('GET', 'POST');

            $app->map('/area/:text/:id', function ($text, $id) use ($app) {
                $data[] = $text;
                $data[] = $id;
                $this->initroute('Site', 'Equipe', 'area', $data);
            })->via('GET', 'POST');

            $app->map('/:text/:id', function ($text, $id) use ($app) {
                $data[] = $text;
                $data[] = $id;
                $this->initroute('Site', 'Equipe', 'cv', $data);
            })->via('GET', 'POST');
        });

        $app->group('/tratamentos', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Site', 'Tratamentos', 'index');
            })->via('GET', 'POST');

            $app->get('/index', function () use ($app) {
                $this->initroute('Site', 'Tratamentos', 'index');
            })->via('GET', 'POST');

            $app->map('/:text/:id', function ($text, $id) use ($app) {
                $data[] = $text;
                $data[] = $id;
                $this->initroute('Site', 'Tratamentos', 'more', $data);
            })->via('GET', 'POST');
        });

        $app->group('/projetos', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Site', 'Projetos', 'index');
            })->via('GET', 'POST');

            $app->get('/index', function () use ($app) {
                $this->initroute('Site', 'Projetos', 'index');
            })->via('GET', 'POST');

            $app->map('/:text/:id', function ($text, $id) use ($app) {
                $data[] = $text;
                $data[] = $id;
                $this->initroute('Site', 'Projetos', 'more', $data);
            })->via('GET', 'POST');
        });

        $app->group('/especialidades', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Site', 'Especialidades', 'index');
            })->via('GET', 'POST');

            $app->get('/index', function () use ($app) {
                $this->initroute('Site', 'Especialidades', 'index');
            })->via('GET', 'POST');

            $app->map('/:text/:id', function ($text, $id) use ($app) {
                $data[] = $text;
                $data[] = $id;
                $this->initroute('Site', 'Especialidades', 'more', $data);
            })->via('GET', 'POST');
        });

        $app->group('/agenda', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Site', 'Agenda', 'index');
            })->via('GET', 'POST');

            $app->get('/index', function () use ($app) {
                $this->initroute('Site', 'Agenda', 'index');
            })->via('GET', 'POST');

            $app->map('/:text/:id', function ($text, $id) use ($app) {
                $data[] = $text;
                $data[] = $id;
                $this->initroute('Site', 'Agenda', 'category', $data);
            })->via('GET', 'POST');
        });
            
        $app->group('/exames', function () use ($app) {
            $app->get('/', function () use ($app) {
                $this->initroute('Site', 'Midia', 'index');
            })->via('GET', 'POST');

            $app->get('/index', function () use ($app) {
                $this->initroute('Site', 'Midia', 'index');
            })->via('GET', 'POST');

            $app->map('/:id', function ($id) use ($app) {
                $data[] = $id;
                $this->initroute('Site', 'Midia', 'index', $data);
            })->via('GET', 'POST');
        });

        $app->get('/contato', function () use ($app) {
            $this->initroute('Site', 'Contato', 'index');
        })->via('GET', 'POST');

        $app->get('/rh', function () use ($app) {
            $data[] = 'rh';
            $this->initroute('Site', 'Contato', 'index', $data);
        })->via('GET', 'POST');

        $app->get('/contato/enviar', function () use ($app) {
            $this->initroute('Site', 'Contato', 'enviar');
        })->via('GET', 'POST');

        $app->get('/search', function () use ($app) {
            $this->initroute('Site', 'Search', 'index');
        })->via('GET', 'POST');


        $app->run();
    }
}
