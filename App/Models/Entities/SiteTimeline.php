<?php

namespace App\Models\Entities;

use App\Core\Models;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class SiteTimeline extends Models {
    protected $table = 'site_timeline';
    protected $primaryKey = 'idtimeline';
    public $timestamps = false;
    protected $fillable = [ 'text', 'icon', 'year', 'status'];
    protected $guarded = [];

}
