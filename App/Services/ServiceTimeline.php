<?php
/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\SiteTimeline;


class ServiceTimeline{

    public function create($request) {
        if($request){
            $obj = new SiteTimeline();
            $this->save($request, $obj);

            $return = [];
            if($obj->idtimeline <> ''){
                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir o Timeline <strong>{$obj->text}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request) {
        if($request){
            $obj = SiteTimeline::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if($obj->idtimeline <> ''){

                $return['success'] = true;
                $return['data'] = $obj;
            }else{
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar o Timeline <strong>{$obj->text}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id){
        $departamento = SiteTimeline::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if($resp){
            $return['success'] = true;
        }else{
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj){
        $obj->text = $request['text'];
        $obj->icon = $request['icon'];
        $obj->year = $request['year'];
        $obj->status = $request['status'];

        $obj->save();

    }

}
