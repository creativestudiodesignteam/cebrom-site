<?php

namespace App\Models\Entities;

use App\Core\Models;
use App\Models\Entities\Team;
use App\Models\Entities\TeamType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arquivos
 *
 * @author oseas
 */
class TeamSpecialties extends Models {
    protected $table = 'team_specialties';
    protected $primaryKey = 'idspecialties';
    public $timestamps = false;
    protected $fillable = ['title', 'status'];
    protected $guarded = [];

    public function teams(){
        return $this->belongsToMany(Team::class, 'teamxspecialties', $this->primaryKey, 'idteam');
    }

    public function types()
    {
        return $this->belongsTo(TeamType::class, 'idtype');
    }

}
