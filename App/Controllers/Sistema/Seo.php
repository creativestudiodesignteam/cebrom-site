<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Models\Entities\SiteSeo;
use App\Services\ServiceSeo;

/**
 * Description of Home
 *
 * @author oseas
 */
class Seo extends Common{

    protected $service;
    protected $pagelink = 'sistema/seo';
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct(){
        $this->start_session();
        $this->view = new \stdClass();
        $this->service = new ServiceSeo();
        $this->obj = new SiteSeo();
        $this->obj->status = 'a';

    }

    public function index() {
        $this->start_session();

        $this->titulo_pagina = 'Seo';
        $obj = SiteSeo::find(1);

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Seo ';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->action = '/' . $this->pagelink . '/index';
        $this->view->url = '/'.$this->pagelink;
        $this->view->titulo = 'Lista de Seo';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;

        if(isset($_SESSION['message'])){
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        if ($_POST) {
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if ($request['success']) {
                $_SESSION['message'] = "Seo Atualizado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/' . $this->pagelink;
            } else {
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        $this->render('index', $this->folder, $this->page);
    }
}
