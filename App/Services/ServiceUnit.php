<?php

/**
 * Created by PhpStorm.
 * User: oseas
 * Date: 08/12/2018
 * Time: 00:38
 */

namespace App\Services;

use \App\Models\Entities\Unit;


class ServiceUnit
{

    public function create($request)
    {
        if ($request) {
            $obj = new Unit();
            $this->save($request, $obj);

            $return = [];
            if ($obj->idunits <> '') {
                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel inserir a Unidade <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function update($request)
    {
        if ($request) {
            $obj = Unit::find($request['id']);
            $this->save($request, $obj);

            $return = [];
            if ($obj->idunits <> '') {

                $return['success'] = true;
                $return['data'] = $obj;
            } else {
                $return['success'] = false;
                $return['message'] = "Não foi possivel atualizar a Unidade <strong>{$obj->title}</strong>";
                $return['data'] = $obj;
            }

            return $return;
        }
    }

    public function destroy($id)
    {
        $departamento = Unit::find($id);
        $departamento->status = 'd';
        $resp = $departamento->save();

        $return = [];

        if ($resp) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
        }

        return $return;
    }

    public function save($request, $obj)
    {
        $obj->title = $request['title'];
        $obj->address = $request['address'];
        $obj->email = $request['email'];
        $obj->phone = $request['phone'];
        if (isset($request['image'])) {
            $obj->image = $request['image'];
        }
        $obj->status = $request['status'];
        $obj->image_title = $request['image_title'];
        $obj->image_alt = $request['image_alt'];
        $obj->operation = $request['operation'];
        $obj->text = $request['text'];
        $obj->idtype = $request['idtype'];

        $obj->save();
    }
}
