<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\SiteBanner;
use \App\Models\Entities\TeamSpecialtieCategory;
use \App\Models\Entities\Treatment;
use \App\Models\Entities\Unit;
use \App\Models\Entities\Team;
use \App\Models\Entities\TeamType;
use \App\Models\Entities\Project;
use \App\Models\Entities\SiteNews;
use \App\Models\Entities\SiteTimeline;

/**
 * Description of Home
 *
 * @author oseas¹
 */
class Home extends Common{

    protected $pagelink = '';

    public function index() {
        $this->start_session();
        $this->view->page = 'header-home';
        $this->view->titlehead = 'Home';
        $this->view->banners = SiteBanner::where('status', '=', 'a')->get();
        $this->view->timeline = SiteTimeline::where('status', '=', 'a')->get();
        $this->view->units = Unit::where('status', '=', 'a')->orderBy('idunits', 'ASC')->get();
        $this->view->unit = Unit::first();
        $this->view->teams = Team::where('status', '=', 'a')->get();
        $this->view->types = TeamType::where('status', '=', 'a')->get();
        $this->view->projects = Project::where('status', '=', 'a')->get();
        $this->view->treatments = Treatment::where('status', '=', 'a')->get();
        $this->view->specialtieCategories = TeamSpecialtieCategory::where('status', '=', 'a')->get();

        $this->render('index', $this->folder, $this->page);
    }

    public function newsletter(){
      if($_POST){
        $issetemail = SiteNews::where('email', '=', $_POST['email'])->count();
        if($issetemail>0){
          $return['response']['mensagem'] = 'E-mail já cadastrado em nossa base!';
          $return['response']['classe'] = 'alert-danger';
          $return['response']['result'] = 'error';
          $return['response']['redirect'] = '';

          print_r(json_encode($return));
          exit();
        }

        if($_POST['name'] == ''){
          $return['response']['mensagem'] = 'O nome não pode estar vazio!';
          $return['response']['classe'] = 'alert-danger';
          $return['response']['result'] = 'error';
          $return['response']['redirect'] = '';

          print_r(json_encode($return));
          exit();
        }

        if ($_POST['email'] == '') {
            $return['response']['mensagem'] = 'E-mail não pode ser em branco!';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';

            print_r(json_encode($return));
            exit();
        }


        $news = new SiteNews();
        $news->email = $_POST['email'];
        $news->name = $_POST['name'];

        $news->data_create = date('Y-m-d H:i:s');
        $save = $news->save();

        if($save){
            $return['response']['mensagem'] = 'E-mail adicionado a base com sucesso!';
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['redirect'] = '/home';
        }else{
            $return['response']['mensagem'] = 'Não foi adicionar o e-mail a nossa base!';
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
        }

        print_r(json_encode($return));
        exit();
      }
    }
}
