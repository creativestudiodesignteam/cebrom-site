<?php

namespace App\Controllers\Sistema;

use \App\Controllers\Sistema\Common;
use \App\Models\Entities\Team;
use \App\Models\Entities\TeamType;
use App\Services\ServiceTeam;
use App\Models\Entities\Unit;
use App\Models\Entities\TeamSpecialties;

/**
 * Description of Home
 *
 * @author oseas
 */
class Teams extends Common{

    protected $service;
    protected $pagelink = 'sistema/team';
    protected $obj;
    protected $id_class = 'usuarios';

    public function __construct(){
        $this->start_session();
        $this->view = new \stdClass();
        $this->service = new ServiceTeam();
        $this->obj = new Team();
        $this->obj->status = 'a';
        $this->view->types = TeamType::where('status', '=', 'a')->get();
        $this->units = Unit::where('status', '=', 'a')->get();
        $this->specialties = TeamSpecialties::where('status', '=', 'a')->get();

    }

    public function index() {
        $this->start_session();

        $this->titulo_pagina = 'Equipe';
        $objs = Team::where('status', '<>', 'd')->get();

        $breadcrumb['url'][0] = '#';
        $breadcrumb['label'][0] = 'Lista de Teams ';
        $breadcrumb['active'][0] = 'active';
        $this->view->breadcrumb = $breadcrumb;
        $this->view->url = '/'.$this->pagelink;
        $this->view->titulo = 'Lista de Equipe';
        $this->view->objs = $objs;

        if(isset($_SESSION['message'])){
            $this->view->errormessage = $_SESSION['message'];
            $this->view->classe = $_SESSION['classe'];
            unset($_SESSION['message']);
            unset($_SESSION['classe']);
        }

        $this->render('index', $this->folder, $this->page);
    }

    public function create(){
        $this->start_session();
        $this->titulo_pagina = 'Equipe - Cadastro';
        $this->id_class  = 'cadastrar-anuncio';

        $units = [];
        foreach ($this->units as $m) {
            $units['unit'][] = $m;
            $units['ckecked'][] = $this->checkunitteam($m->idunits, $this->obj->idteam);
        }

        $this->view->units = $units;

        $specialties = [];
        foreach ($this->specialties as $m) {
            $specialties['specialties'][] = $m;
            $specialties['ckecked'][] = $this->checkspecialtiesteam($m->idspecialties, $this->obj->idteam);
        }

        $this->view->specialties = $specialties;

        $breadcrumb['url'][0] = '/'.$this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Equipe';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Cadastrar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/'.$this->pagelink;
        $this->view->action = '/'.$this->pagelink.'/create';
        $this->view->titulo = 'Lista de Equipe';
        $this->view->label = 'Cadastrar';
        $this->view->class_btn = 'btn btn-success';
        $this->view->obj = $this->obj;

        if($_POST){
            $request = $this->service->create($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if($request['success']){
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> Inserido com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/'.$this->pagelink;
            }else{
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }

        $this->render('form', $this->folder, $this->page);
    }

    public function update($id, $data = []){
        $this->start_session();
        $this->titulo_pagina = 'Equipe - Editar';
        $this->id_class  = 'cadastrar-anuncio';
        $obj = Team::find($id);

        $units = [];
        foreach ($this->units as $m) {
            $units['unit'][] = $m;
            $units['ckecked'][] = $this->checkunitteam($m->idunits, $obj->idteam);
        }

        $this->view->units = $units;


        $specialties = [];
        foreach ($this->specialties as $m) {
            $specialties['specialties'][] = $m;
            $specialties['ckecked'][] = $this->checkspecialtiesteam($m->idspecialties, $obj->idteam);
        }

        $this->view->specialties = $specialties;

        $breadcrumb['url'][0] = '/'.$this->pagelink;
        $breadcrumb['label'][0] = 'Lista de Equipe ';
        $breadcrumb['active'][0] = '';
        $breadcrumb['url'][1] = '#';
        $breadcrumb['label'][1] = 'Alterar';
        $breadcrumb['active'][1] = 'active';
        $this->view->breadcrumb = $breadcrumb;

        $this->view->url = '/'.$this->pagelink;
        $this->view->action = '/'.$this->pagelink.'/update/'.$id;
        $this->view->titulo = 'Lista de Equipe';
        $this->view->label = 'Alterar';
        $this->view->class_btn = 'btn btn-warning';
        $this->view->obj = $obj;

        if($_POST){
            $request = $this->service->update($_POST);
            $obj = $request['success'] ? $request['data'] : null;

            if($request['success']){
                $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> alterado com sucesso!";
                $_SESSION['classe'] = 'alert-success';
                $return['response']['mensagem'] = $_SESSION['message'];
                $return['response']['classe'] = $_SESSION['classe'];
                $return['response']['result'] = 'success';
                $return['response']['redirect'] = '/'.$this->pagelink;
            }else{
                $return['response']['mensagem'] = $request['message'];
                $return['response']['classe'] = 'alert-danger';
                $return['response']['result'] = 'error';
                $return['response']['redirect'] = '';
            }

            print_r(json_encode($return));
            exit();
        }


        $this->render('form', $this->folder, $this->page);
    }

    public function destroy($id){
        $this->start_session();
        $obj = Team::find($id);
        $request = $this->service->destroy($id);

        if($request['success']){
            $_SESSION['message'] = "Registro <strong>{$obj->title}</strong> excluida com sucesso!";
            $_SESSION['classe'] = 'alert-success';
        }else{
            $_SESSION['message'] = "Não foi possivel excluir a Team <strong>{$obj->title}</strong>";
            $_SESSION['classe'] = 'alert-danger';
        }

        print_r(json_encode($request));
        exit();

    }
    
    public function uploads(){
      if(isset($_FILES['file']['name']) && $_FILES['file']['name'] <> ''){
        $responseimg = $this->upload($_FILES, 'team');

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }

    public function uploadfile(){
      if(isset($_FILES['file']['name']) && $_FILES['file']['name'] <> ''){
        $responseimg = $this->upload($_FILES, 'team');

        $obj = Team::find($_GET['id']);
        $obj->cv = $responseimg['image'];
        $obj->save();

        if($responseimg['success']){
            $return['response']['classe'] = 'alert-success';
            $return['response']['result'] = 'success';
            $return['response']['mensagem'] = 'Arquivo enviado com sucesso!';
            $return['response']['image'] = $responseimg['image'];
            $return['response']['status'] = 1;
            $return['response']['redirect'] = '/'.$this->pagelink;
        }else{
            $return['response']['classe'] = 'alert-danger';
            $return['response']['result'] = 'error';
            $return['response']['mensagem'] = 'Não foi possivel subir o arquivo!';
            $return['response']['redirect'] = '';
            $return['response']['status'] = 0;
        }

        $return['response']['mensagem'] = $responseimg['message'];
        print_r(json_encode($return));
        exit();
      }
    }

    public function checkunitteam($unit, $team)
    {
        $return = Team::join( 'teamxunit', 'team.idteam', '=', 'teamxunit.idteam')
            ->where( 'teamxunit.idunits', '=', $unit)->where( 'teamxunit.idteam', '=', $team)
            ->select( 'teamxunit.idteam')->count();

        if ($return > 0) {
            $checked = 'checked="checked"';
        } else {
            $checked =  '';
        }

        return $checked;
    }

    public function checkspecialtiesteam($specialties, $team)
    {
        $return = Team::join('teamxspecialties', 'team.idteam', '=', 'teamxspecialties.idteam')
            ->where('teamxspecialties.idspecialties', '=', $specialties)->where('teamxspecialties.idteam', '=', $team)
            ->select('teamxspecialties.idteam')->count();

        if ($return > 0) {
            $checked = 'checked="checked"';
        } else {
            $checked =  '';
        }

        return $checked;
    }

}
