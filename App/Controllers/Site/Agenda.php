<?php

namespace App\Controllers\Site;

use \App\Controllers\Site\Common;
use \App\Models\Entities\Schedule;
use \App\Models\Entities\ScheduleCategory;

/**
 * Description of Empresa
 *
 * @author oseas¹
 */
class Agenda extends Common{

    protected $pagelink = '';
    protected $title = 'comunicação';
    protected $titlesub = 'agenda de';
    protected $pagename = 'Eventos';
    protected $bread = '';

    public function index($id = '') {
        $this->start_session();
        $this->view->page = 'header-agenda';
        $this->view->categories = ScheduleCategory::where('status', '=', 'a')->get();
        
        if($id == ''){
            $this->view->schedules = Schedule::where('status', '=', 'a')->get();
            $this->view->category = new ScheduleCategory();
        }else{
            $this->view->schedules = Schedule::where('status', '=', 'a')->where('idcategory', '=', $id)->get();
            $this->view->category = ScheduleCategory::find($id);
        }

        $this->view->titlehead = 'Agenda de Eventos';
        $this->render('index', $this->folder, $this->page);
    }

    public function category($text = '', $id = ''){
        $this->index($id);
        exit();
    }
}
